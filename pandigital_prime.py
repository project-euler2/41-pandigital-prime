import itertools
import time
import math
start_time = time.time()

def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def pandigital_prime(n_max_length):
    largest_pandigital = 0
    for i in range(2,n_max_length):
        all_options = []
        number_str = int("".join([str(integer) for integer in list(range(1,i))]))
        all_options = list(itertools.permutations(str(number_str)))
        for option in all_options:
            if is_prime(int("".join([str(integer) for integer in list(option)]))):
                if int("".join([str(integer) for integer in list(option)])) > largest_pandigital:
                    largest_pandigital = int("".join([str(integer) for integer in list(option)]))
    return largest_pandigital
print(pandigital_prime(10))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )